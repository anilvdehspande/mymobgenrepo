package com.shell.assessmentapp.domain.repository

import com.shell.assessmentapp.data.model.Book

interface BooksRepository {
    interface CallBack{
        fun  onSuccess(categories: List<Book>)
        fun  onFailure(errorMessage: String)
    }
    fun getBooks(): Unit

    fun setCallBackInstance(callBack: BooksRepository.CallBack): Unit
}