package com.shell.assessmentapp.domain.api

import com.shell.assessmentapp.data.model.Book
import com.shell.assessmentapp.data.model.Category
import com.shell.assessmentapp.data.model.House
import com.shell.assessmentapp.data.model.Charachter
import retrofit2.http.GET

interface ApiService {
    @GET("categories")
    suspend fun getCategories(): List<Category>

    @GET("list/1")
    suspend fun getBooks(): List<Book>

    @GET("list/2")
    suspend fun getHouses(): List<House>

    @GET("list/3")
    suspend fun getCharacters(): List<Charachter>
}