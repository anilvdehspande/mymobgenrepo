package com.shell.assessmentapp.domain.interactor.imp

import com.shell.assessmentapp.domain.executor.Executor
import com.shell.assessmentapp.domain.executor.MainThread
import com.shell.assessmentapp.domain.interactor.CharsInteractor
import com.shell.assessmentapp.domain.interactor.base.AbstractInteractor
import com.shell.assessmentapp.data.model.Charachter
import com.shell.assessmentapp.domain.repository.CharsRepository

class CharsInteractorImp (threadExecutor: Executor,
                          mainThread: MainThread,
                          callback: CharsInteractor.Callback,
                          repository: CharsRepository
): AbstractInteractor(threadExecutor, mainThread), CharsInteractor, CharsRepository.CallBack {

    var repository: CharsRepository? = repository
    var callback: CharsInteractor.Callback? =callback

    init {
        this.repository?.setCallBackInstance(this)
    }

    override fun run() {
        var chars = repository?.getAllCharacters()

        if(chars==null) {
            notifyError()
            return
        }
    }

    private fun postCharachters(charachters: List<Charachter>) {
        mainThread?.post(Runnable { callback?.onCharsReceived(charachters) })
    }

    private fun notifyError() {
        mainThread?.post(Runnable { callback?.onCharsFailed("Something went wrong") })
    }

    override fun onFailure(errorMessage: String) {
        notifyError()
    }

    override fun onSuccess(charachters: List<Charachter>) {
        postCharachters(charachters)
    }
}