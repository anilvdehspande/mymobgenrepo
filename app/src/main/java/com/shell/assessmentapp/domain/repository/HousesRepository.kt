package com.shell.assessmentapp.domain.repository

import com.shell.assessmentapp.data.model.House

interface HousesRepository {
    interface CallBack{
        fun  onSuccess(categories: List<House>)
        fun  onFailure(errorMessage: String)
    }
    fun getHouses(): Unit

    fun setCallBackInstance(callBack: HousesRepository.CallBack): Unit
}