package com.shell.assessmentapp.domain.executor.imp

import com.shell.assessmentapp.domain.executor.Executor
import com.shell.assessmentapp.domain.interactor.base.AbstractInteractor
import java.util.concurrent.BlockingQueue
import java.util.concurrent.LinkedBlockingQueue
import java.util.concurrent.ThreadPoolExecutor
import java.util.concurrent.TimeUnit

public object ThreadExecutorSingleton: Executor {

    private val CORE_POOL_SIZE = 3
    private val MAX_POOL_SIZE = 5
    private val KEEP_ALIVE_TIME = 120
    private val TIME_UNIT = TimeUnit.SECONDS
    private val WORK_QUEUE: BlockingQueue<Runnable> = LinkedBlockingQueue();

    private val mThreadPoolExecutor: ThreadPoolExecutor  = ThreadPoolExecutor(CORE_POOL_SIZE, MAX_POOL_SIZE, KEEP_ALIVE_TIME.toLong(), TIME_UNIT, WORK_QUEUE)

    override fun execute(interactor: AbstractInteractor) {
        mThreadPoolExecutor.submit(Runnable {
            interactor.run()
            interactor.onFinished()
        })
    }

}