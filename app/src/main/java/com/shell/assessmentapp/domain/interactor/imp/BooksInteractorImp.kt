package com.shell.assessmentapp.domain.interactor.imp

import com.shell.assessmentapp.domain.executor.Executor
import com.shell.assessmentapp.domain.executor.MainThread
import com.shell.assessmentapp.domain.interactor.BooksInteractor
import com.shell.assessmentapp.domain.interactor.CategoryInteractor
import com.shell.assessmentapp.domain.interactor.base.AbstractInteractor
import com.shell.assessmentapp.data.model.Book
import com.shell.assessmentapp.domain.repository.BooksRepository
import org.koin.core.KoinComponent
import org.koin.core.inject

class BooksInteractorImp (threadExecutor: Executor,
                          mainThread: MainThread,
                          callback: BooksInteractor.Callback,
                          repository: BooksRepository): AbstractInteractor(threadExecutor, mainThread), BooksInteractor, BooksRepository.CallBack, KoinComponent {

    val repository: BooksRepository? by inject()
    var callback: BooksInteractor.Callback? = callback

    init {
        this.repository?.setCallBackInstance(this)
    }

    override fun run() {
        var books = repository?.getBooks()

        if(books==null) {
            notifyError()
            return
        }
    }

    private fun postCategories(books: List<Book>) {
        mainThread?.post(Runnable { callback?.onBooksReceived(books) })
    }

    private fun notifyError() {
        mainThread?.post(Runnable { callback?.onBooksFailed("Something went wrong") })
    }

    override fun onFailure(errorMessage: String) {
        notifyError()
    }

    override fun onSuccess(books: List<Book>) {
        postCategories(books)
    }
}