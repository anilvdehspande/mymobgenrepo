package com.shell.assessmentapp.domain.repository

import com.shell.assessmentapp.data.model.Charachter

interface CharsRepository {

    interface CallBack{
        fun  onSuccess(charachters: List<Charachter>)
        fun  onFailure(errorMessage: String)
    }

    fun getAllCharacters(): Unit

    fun setCallBackInstance(callBack: CharsRepository.CallBack): Unit
}