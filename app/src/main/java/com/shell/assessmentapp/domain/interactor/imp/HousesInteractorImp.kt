package com.shell.assessmentapp.domain.interactor.imp

import com.shell.assessmentapp.domain.executor.Executor
import com.shell.assessmentapp.domain.executor.MainThread
import com.shell.assessmentapp.domain.interactor.CategoryInteractor
import com.shell.assessmentapp.domain.interactor.HousesInteractor
import com.shell.assessmentapp.domain.interactor.base.AbstractInteractor
import com.shell.assessmentapp.data.model.House
import com.shell.assessmentapp.domain.repository.HousesRepository

class HousesInteractorImp (threadExecutor: Executor,
                           mainThread: MainThread,
                           callback: HousesInteractor.Callback,
                           repository: HousesRepository
): AbstractInteractor(threadExecutor, mainThread), HousesInteractor, HousesRepository.CallBack {

    var repository: HousesRepository? = repository
    var callback: HousesInteractor.Callback? =callback

    init {
        this.repository?.setCallBackInstance(this)
    }

    override fun run() {
        var houses = repository?.getHouses()

        if(houses==null) {
            notifyError()
            return
        }
    }

    private fun postHouses(houses: List<House>) {
        mainThread?.post(Runnable { callback?.onHousesReceived(houses) })
    }

    private fun notifyError() {
        mainThread?.post(Runnable { callback?.onHousesFailed("Something went wrong") })
    }

    override fun onFailure(errorMessage: String) {
        notifyError()
    }

    override fun onSuccess(houses: List<House>) {
        postHouses(houses)
    }
}