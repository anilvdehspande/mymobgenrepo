package com.shell.assessmentapp.domain.repository

import androidx.lifecycle.LiveData
import com.shell.assessmentapp.data.model.Category

interface CategoryRepository {
    interface CallBack{
        fun  onSuccess(categories: List<Category>)
        fun  onFailure(errorMessage: String)
    }
    fun getCategories(): Unit
    fun setCallBackInstance(callBack: CallBack): Unit
}