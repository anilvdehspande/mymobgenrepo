package com.shell.assessmentapp.domain.repository.imp

import android.content.Context
import android.util.Log
import com.shell.assessmentapp.domain.api.MyRetrofitBuilder
import com.shell.assessmentapp.data.model.Charachter
import com.shell.assessmentapp.domain.repository.CharsRepository
import com.shell.assessmentapp.data.storage.cache.CharactersDbCache
import com.shell.assessmentapp.data.storage.cache.impl.CharactersDbCacheImpl
import kotlinx.coroutines.*

class CharsRepositoryImpl(context: Context) : CharsRepository {

    val TAG = "Repository"

    private var charDbCache: CharactersDbCache? = CharactersDbCacheImpl.getInstance(context.applicationContext)
    protected var chacracters: List<Charachter>?  = null
    private var callbackInstance: CharsRepository.CallBack? = null

    var job: CompletableJob? = null

    override fun getAllCharacters(): Unit {

        job = Job()
        job?.let {job ->
            CoroutineScope(Dispatchers.IO +job).launch {
                if(charDbCache?.isDataCached()!!){
                    chacracters = charDbCache?.getAllCharacters()
                    Log.i(TAG, "CharsRepositoryImpl -> Getting from cache: ${chacracters?.size}")
                }else{
                    var temp  =  MyRetrofitBuilder.apiService.getCharacters()
                    charDbCache?.insertCharacters(temp)
                    chacracters = charDbCache?.getAllCharacters()
                    Log.i(TAG, "CharsRepositoryImpl -> Getting from API: ${chacracters?.size}")
                }
                withContext(Dispatchers.Main){
                    job.complete()
                    if(chacracters?.size == 0){
                        callbackInstance?.onFailure("CharsRepositoryImpl -> Could not fetch")
                    }else{
                        callbackInstance?.onSuccess(chacracters!!)
                    }
                }
            }
        }
    }

    override fun setCallBackInstance(callBack: CharsRepository.CallBack) {
        this.callbackInstance = callBack
    }

    fun cancelJobs(){
        job?.cancel()
    }
}