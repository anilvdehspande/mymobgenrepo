package com.shell.assessmentapp.domain.interactor

import com.shell.assessmentapp.domain.interactor.base.Interactor
import com.shell.assessmentapp.data.model.Charachter

interface CharsInteractor: Interactor {
    interface Callback{
        fun onCharsReceived(charachters: List<Charachter>)
        fun onCharsFailed(error: String)
    }
}