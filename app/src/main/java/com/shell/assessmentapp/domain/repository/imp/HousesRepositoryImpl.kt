package com.shell.assessmentapp.domain.repository.imp

import android.content.Context
import android.util.Log
import com.shell.assessmentapp.domain.api.MyRetrofitBuilder
import com.shell.assessmentapp.data.model.House
import com.shell.assessmentapp.domain.repository.HousesRepository
import com.shell.assessmentapp.data.storage.cache.HousesDbCache
import com.shell.assessmentapp.data.storage.cache.impl.HousesDbCacheImpl
import kotlinx.coroutines.*

class HousesRepositoryImpl(context: Context): HousesRepository {

    val TAG = "Repository"

    private var housesDbCache:  HousesDbCache? = HousesDbCacheImpl.getInstance(context.applicationContext)
    private var houses: List<House>? = null
    private var callBackInstance: HousesRepository.CallBack? = null

    var job: CompletableJob? = null

    override fun getHouses(){
        job = Job()
        job?.let {job ->
            CoroutineScope(Dispatchers.IO +job).launch {
                if(housesDbCache?.isDataCached()!!){
                    houses = housesDbCache?.getAllHouses()
                    Log.i(TAG, "HousesRepositoryImpl -> Getting from cache: ${houses?.size}")
                }else{
                    var temp  =  MyRetrofitBuilder.apiService.getHouses()
                    housesDbCache?.insertHouses(temp)
                    houses = housesDbCache?.getAllHouses()
                    Log.i(TAG, "HousesRepositoryImpl -> Getting from API: ${houses?.size}")
                }
                withContext(Dispatchers.Main){
                    job.complete()
                    if(houses?.size == 0){
                        callBackInstance?.onFailure("HousesRepositoryImpl -> Could not fetch")
                    }else{
                        callBackInstance?.onSuccess(houses!!)
                    }
                }
            }
        }
    }

    override fun setCallBackInstance(callBack: HousesRepository.CallBack) {
        this.callBackInstance = callBack
    }

    fun cancelJobs(){
        job?.cancel()
    }

}