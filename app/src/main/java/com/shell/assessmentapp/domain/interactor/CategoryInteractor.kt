package com.shell.assessmentapp.domain.interactor

import com.shell.assessmentapp.domain.interactor.base.Interactor
import com.shell.assessmentapp.data.model.Category

interface CategoryInteractor: Interactor {
    interface CallBack{
        fun onCategoriesReceived(categories: List<Category>)
        fun onCategoriesFailed(error: String)
    }
}