package com.shell.assessmentapp.domain.api

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object MyRetrofitBuilder {
    const val BASE_URL = "https://private-anon-4b858cf5ce-androidtestmobgen.apiary-mock.com/"

    val retrofiBuilder: Retrofit.Builder by lazy {
        Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
    }

    val apiService: ApiService by lazy {
        retrofiBuilder.build().create(ApiService::class.java)
    }
}