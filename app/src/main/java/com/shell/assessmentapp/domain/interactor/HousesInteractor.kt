package com.shell.assessmentapp.domain.interactor

import com.shell.assessmentapp.domain.interactor.base.Interactor
import com.shell.assessmentapp.data.model.House

interface HousesInteractor: Interactor {
    interface Callback{
        fun onHousesReceived(houses: List<House>)
        fun onHousesFailed(error: String)
    }
}