package com.shell.assessmentapp.domain.interactor.imp

import com.shell.assessmentapp.data.model.Category
import com.shell.assessmentapp.domain.executor.Executor
import com.shell.assessmentapp.domain.executor.MainThread
import com.shell.assessmentapp.domain.interactor.CategoryInteractor
import com.shell.assessmentapp.domain.interactor.base.AbstractInteractor
import com.shell.assessmentapp.domain.repository.CategoryRepository

class CategoryInteractorImpl(threadExecutor: Executor,
                         mainThread: MainThread,
                         callback: CategoryInteractor.CallBack,
                         repository: CategoryRepository): AbstractInteractor(threadExecutor, mainThread), CategoryInteractor, CategoryRepository.CallBack {

    var repository: CategoryRepository? = repository
    var callback: CategoryInteractor.CallBack? = callback

    init {
        this.repository?.setCallBackInstance(this)
    }

    override fun run() {
        var categories = repository?.getCategories()

        if(categories==null) {
            notifyError()
            return
        }
    }

    private fun postCategories(cagories: List<Category>){
        mainThread?.post(Runnable { callback?.onCategoriesReceived(cagories) })
    }

    private fun notifyError() {
        mainThread?.post(Runnable { callback?.onCategoriesFailed("Something went wrong") })
    }

    override fun onSuccess(categories: List<Category>) {
        postCategories(categories)
    }

    override fun onFailure(errorMessage: String) {
        notifyError()
    }
}