package com.shell.assessmentapp.domain.repository.imp

import android.content.Context
import android.util.Log
import com.shell.assessmentapp.domain.api.MyRetrofitBuilder
import com.shell.assessmentapp.data.model.Category
import com.shell.assessmentapp.domain.repository.CategoryRepository
import com.shell.assessmentapp.data.storage.cache.CategoryDbCache
import com.shell.assessmentapp.data.storage.cache.impl.CategoryDbCacheImpl
import kotlinx.coroutines.*
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.Dispatchers.Main


class CategoryRepositoryImp(context: Context): CategoryRepository {

    val TAG = "Repository"

    private var categoryDbCache: CategoryDbCache? = CategoryDbCacheImpl.getInstance(context.applicationContext)
    private var categories: List<Category>? = null
    private var callbackInstance: CategoryRepository.CallBack ? = null

    override fun setCallBackInstance(callBack: CategoryRepository.CallBack) {
        callbackInstance = callBack
    }

    var job: CompletableJob? = null

    override fun getCategories(): Unit {
        job = Job()
        job?.let {job ->
            CoroutineScope(IO +job).launch {
                if(categoryDbCache?.isDataCached()!!){
                    categories = categoryDbCache?.getCallCategories()
                    Log.i(TAG, "CategoryRepositoryImp -> Getting from cache: ${categories?.size}")
                }else{
                    var temp  =  MyRetrofitBuilder.apiService.getCategories()
                    categoryDbCache?.insertCategories(temp)
                    categories = categoryDbCache?.getCallCategories()
                    Log.i(TAG, "CategoryRepositoryImp -> Getting from API: ${categories?.size}")
                }
                withContext(Main){
                    job.complete()
                    if(categories?.size == 0){
                        callbackInstance?.onFailure("CategoryRepositoryImp -> Could not fetch")
                    }else{
                        callbackInstance?.onSuccess(categories!!)
                    }
                }
            }
        }
    }

    fun cancelJobs(){
        job?.cancel()
    }

}