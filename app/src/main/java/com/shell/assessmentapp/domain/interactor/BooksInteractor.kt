package com.shell.assessmentapp.domain.interactor

import com.shell.assessmentapp.domain.interactor.base.Interactor
import com.shell.assessmentapp.data.model.Book

interface BooksInteractor: Interactor {
    interface Callback{
        fun onBooksReceived(books: List<Book>)
        fun onBooksFailed(error: String)
    }
}