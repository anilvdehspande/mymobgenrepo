package com.shell.assessmentapp.domain.executor

import com.shell.assessmentapp.domain.interactor.base.AbstractInteractor

interface Executor {
    fun execute(interactor: AbstractInteractor): Unit
}