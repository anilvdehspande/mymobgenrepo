package com.shell.assessmentapp.domain.executor

public interface MainThread {
    fun post(runnable: Runnable): Unit
}