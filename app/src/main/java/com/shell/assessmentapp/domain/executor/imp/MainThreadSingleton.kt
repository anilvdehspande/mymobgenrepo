package com.shell.assessmentapp.domain.executor.imp

import android.os.Handler
import android.os.Looper
import com.shell.assessmentapp.domain.executor.MainThread

object MainThreadSingleton : MainThread{

    var handler: Handler = Handler(Looper.getMainLooper())

    override fun post(runnable: Runnable) {
        handler.post(runnable)
    }
}