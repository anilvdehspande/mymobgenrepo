package com.shell.assessmentapp.domain.repository.imp

import android.content.Context
import android.util.Log
import com.shell.assessmentapp.domain.api.MyRetrofitBuilder
import com.shell.assessmentapp.data.model.Book
import com.shell.assessmentapp.domain.repository.BooksRepository
import com.shell.assessmentapp.data.storage.cache.BookDbCache
import com.shell.assessmentapp.data.storage.cache.impl.BookDbCacheImpl
import kotlinx.coroutines.*
import org.koin.core.KoinComponent
import org.koin.core.inject

class BookRepositoryImp (booksDbCache: BookDbCache): BooksRepository, KoinComponent {

    val TAG = "Repository"

    private val booksDbCache: BookDbCache? by inject()
    private var books: List<Book>? = null
    private var callbackInstance: BooksRepository.CallBack? = null

    override fun setCallBackInstance(callBack: BooksRepository.CallBack) {
        this.callbackInstance = callBack
    }

    var job: CompletableJob? = null

    override fun getBooks(){
        job = Job()
        job?.let {job ->
            CoroutineScope(Dispatchers.IO +job).launch {
                if(booksDbCache?.isDataCached()!!){
                    books = booksDbCache?.getAllBooks()
                    Log.i(TAG, "BookRepositoryImpcache ->Getting from cache: ${books?.size}")
                }else{
                    var temp  =  MyRetrofitBuilder.apiService.getBooks()
                    booksDbCache?.insertBooks(temp)
                    books = booksDbCache?.getAllBooks()
                    Log.i(TAG, "BookRepositoryImpcache -> Getting from API: ${books?.size}")
                }
                withContext(Dispatchers.Main){
                    job.complete()
                    if(books?.size == 0){
                        callbackInstance?.onFailure("BookRepositoryImpcache -> Could not fetch")
                    }else{
                        callbackInstance?.onSuccess(books!!)
                    }
                }
            }
        }
    }

    fun cancelJobs(){
        job?.cancel()
    }

}