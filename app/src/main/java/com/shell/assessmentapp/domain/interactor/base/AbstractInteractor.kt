package com.shell.assessmentapp.domain.interactor.base

import com.shell.assessmentapp.domain.executor.Executor
import com.shell.assessmentapp.domain.executor.MainThread

abstract class AbstractInteractor: Interactor {

    var threadExecutor: Executor? = null
    var mainThread: MainThread? = null

    private var isCancelled: Boolean = false
    private var isRunning: Boolean = false


    constructor(threadExecutor: Executor, mainThread: MainThread){
        this.threadExecutor = threadExecutor
        this.mainThread = mainThread
    }

    abstract fun run(): Unit

    fun cancel(){
        isCancelled = true
        isRunning = false
    }

    fun isRunning(): Boolean? {
        return isRunning
    }

    fun onFinished(){
        isCancelled = false
        isRunning = false
    }

    override fun execute() {
        this.isRunning = true
        threadExecutor?.execute(this)
    }
 }