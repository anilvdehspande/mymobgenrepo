package com.shell.assessmentapp.domain.interactor.base

interface Interactor {
    fun execute(): Unit
}