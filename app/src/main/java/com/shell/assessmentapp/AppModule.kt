package com.shell.assessmentapp

import android.os.Looper
import androidx.room.Room
import androidx.room.RoomDatabase
import com.shell.assessmentapp.data.model.Category
import com.shell.assessmentapp.data.storage.cache.BookDbCache
import com.shell.assessmentapp.data.storage.cache.CategoryDbCache
import com.shell.assessmentapp.data.storage.cache.CharactersDbCache
import com.shell.assessmentapp.data.storage.cache.HousesDbCache
import com.shell.assessmentapp.data.storage.cache.impl.BookDbCacheImpl
import com.shell.assessmentapp.data.storage.cache.impl.CategoryDbCacheImpl
import com.shell.assessmentapp.data.storage.cache.impl.CharactersDbCacheImpl
import com.shell.assessmentapp.data.storage.cache.impl.HousesDbCacheImpl
import com.shell.assessmentapp.data.storage.db.CategoryDatabase
import com.shell.assessmentapp.domain.executor.Executor
import com.shell.assessmentapp.domain.executor.imp.ThreadExecutorSingleton
import com.shell.assessmentapp.domain.repository.CategoryRepository
import com.shell.assessmentapp.domain.repository.CharsRepository
import com.shell.assessmentapp.domain.repository.HousesRepository
import com.shell.assessmentapp.domain.repository.imp.CategoryRepositoryImp
import com.shell.assessmentapp.domain.repository.imp.CharsRepositoryImpl
import com.shell.assessmentapp.domain.repository.imp.HousesRepositoryImpl
import com.shell.assessmentapp.domain.executor.imp.MainThreadSingleton
import com.shell.assessmentapp.domain.repository.BooksRepository
import com.shell.assessmentapp.domain.repository.imp.BookRepositoryImp
import com.shell.assessmentapp.domain.executor.MainThread
import com.shell.assessmentapp.presentation.presenters.*
import com.shell.assessmentapp.presentation.presenters.imp.*
import com.shell.assessmentapp.presentation.ui.activities.MainActivity
import com.shell.assessmentapp.presentation.ui.activities.SplashActivity
import org.koin.android.ext.koin.androidApplication


import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module
import java.util.concurrent.BlockingQueue
import java.util.concurrent.LinkedBlockingQueue
import java.util.concurrent.ThreadPoolExecutor
import java.util.concurrent.TimeUnit

val repositoryModule = module {
    single<CategoryRepository> { CategoryRepositoryImp(androidContext()) }
    single<HousesRepository> { HousesRepositoryImpl(androidContext()) }
    single<CharsRepository> { CharsRepositoryImpl(androidContext()) }
    single<BooksRepository> { BookRepositoryImp(get()) }
}

val executorsModule = module {
    single<Executor> { ThreadExecutorSingleton }
    single<MainThread> { MainThreadSingleton }


}

val presentationModule = module {
    factory<MainActivityPresenter> { (view: MainActivityPresenter.View) -> MainActivityPresenterImpl(view) }
    factory<CategoryPresenter.ScreenSelectionListener> {(view: MainActivityPresenter.View) -> MainActivityPresenterImpl(view)}

    factory<CategoryPresenter> { (view: CategoryPresenter.View) -> CategoryPresenterImp(get(), get(),view, get(), get()) }
    factory<BooksPresenter> { (view: BooksPresenter.View) -> BooksPresenterImpl(get(), get(), view, get() ) }
    factory<HousesPresenter> { (view: HousesPresenter.View) -> HousesPresenterImpl(get(), get(), view, get()) }
    factory<CharactersPresenter> { (view: CharactersPresenter.View) -> CharactersPresenterImpl(get(), get(), view, get())  }
}

val appModule = module {
    single { Room.databaseBuilder(androidContext(), CategoryDatabase::class.java, "category_database").build() }
    single { get<CategoryDatabase>().getHousesDao() }
    single { get<CategoryDatabase>().getCharactersDao() }
    single { get<CategoryDatabase>().getCategoryDao() }
    single { get<CategoryDatabase>().getBookDao() }

    single<BookDbCache> { BookDbCacheImpl(get()) }
    single <CategoryDbCache> { CategoryDbCacheImpl.getInstance(get()) }
    single <HousesDbCache> {HousesDbCacheImpl.getInstance(get())}
    single <CharactersDbCache> { CharactersDbCacheImpl.getInstance(get())}


}
