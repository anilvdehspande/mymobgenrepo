package com.shell.assessmentapp.data.storage.cache.impl

import android.content.Context
import com.shell.assessmentapp.data.model.Book
import com.shell.assessmentapp.data.storage.cache.BookDbCache
import com.shell.assessmentapp.data.storage.db.BooksDao
import com.shell.assessmentapp.data.storage.db.CategoryDatabase
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.koin.core.KoinComponent
import org.koin.core.inject

class BookDbCacheImpl(booksDao: BooksDao) : BookDbCache, KoinComponent {

    private val bookDao: BooksDao? by inject()
    private var books: List<Book>? = listOf()

    init {
        CoroutineScope(Dispatchers.IO).launch {
            books = bookDao?.getAllBooks()
        }
    }

    override suspend fun getAllBooks(): List<Book> {
        return books!!
    }

    override suspend fun insertBooks(array: List<Book>) {
        bookDao?.clean()
        for(book in array){
            bookDao?.insert(book)
        }
        books = bookDao?.getAllBooks()
    }

    override suspend fun isDataCached(): Boolean {
        if(isNull()){
            return false
        }else{
            return true
        }
    }

    private fun isNull() = books == null || books?.size == 0
}