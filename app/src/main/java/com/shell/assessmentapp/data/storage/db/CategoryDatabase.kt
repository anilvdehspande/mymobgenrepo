package com.shell.assessmentapp.data.storage.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.shell.assessmentapp.data.model.Book
import com.shell.assessmentapp.data.model.Category
import com.shell.assessmentapp.data.model.Charachter
import com.shell.assessmentapp.data.model.House
import com.shell.assessmentapp.data.model.converters.CustomConverter

@Database(entities = arrayOf(Category::class, Book::class, House::class, Charachter::class), version = 1, exportSchema = false)
@TypeConverters(CustomConverter::class)
abstract class CategoryDatabase : RoomDatabase() {

    abstract fun getCategoryDao(): CategoryDao
    abstract fun getBookDao(): BooksDao
    abstract fun getHousesDao(): HouseDao
    abstract fun getCharactersDao(): CharcterDao

    companion object {
        @Volatile
        private var INSTANCE: CategoryDatabase? = null
        fun getDatabase(context: Context): CategoryDatabase {
            val tempInstance =
                INSTANCE
            if(tempInstance!=null){
                return tempInstance
            }
            synchronized(this){
                val instance = Room.databaseBuilder(context.applicationContext, CategoryDatabase::class.java, "category_database").build()
                INSTANCE = instance
                return instance
            }
        }
    }
}