package com.shell.assessmentapp.data.storage.cache

import com.shell.assessmentapp.data.model.Book
import com.shell.assessmentapp.data.model.House

interface HousesDbCache {
    suspend fun getAllHouses(): List<House>
    suspend fun insertHouses(houses: List<House>)
    suspend fun isDataCached(): Boolean
}