package com.shell.assessmentapp.data.storage.cache.impl

import android.content.Context
import com.shell.assessmentapp.data.model.Category
import com.shell.assessmentapp.data.storage.cache.CategoryDbCache
import com.shell.assessmentapp.data.storage.db.CategoryDao
import com.shell.assessmentapp.data.storage.db.CategoryDatabase
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

object CategoryDbCacheImpl: CategoryDbCache {

    private var categoryDao: CategoryDao? = null
    var categorys: List<Category>? = null

    fun getInstance(context: Context): CategoryDbCache {
        categoryDao = CategoryDatabase.getDatabase(context.applicationContext).getCategoryDao()
        CoroutineScope(Dispatchers.IO).launch {
            categorys = categoryDao?.getAllCategories()
        }
        return this
    }

    override suspend fun getCallCategories(): List<Category> {
        return categorys!!
    }

    override suspend fun insertCategories(array: List<Category>) {
        categoryDao?.clean()
        for(category in array){
            categoryDao?.insert(category)
        }
        categorys = categoryDao?.getAllCategories()
    }

    override suspend fun isDataCached(): Boolean {
        if(isNull()){
            return false
        }else{
            return true
        }
    }

    private fun isNull() = categorys == null || categorys?.size == 0
}