package com.shell.assessmentapp.data.storage.cache

import com.shell.assessmentapp.data.model.Category

interface CategoryDbCache{
    suspend fun getCallCategories(): List<Category>
    suspend fun insertCategories(array: List<Category>)
    suspend fun isDataCached(): Boolean
}