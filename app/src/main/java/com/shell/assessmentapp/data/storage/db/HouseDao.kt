package com.shell.assessmentapp.data.storage.db

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.shell.assessmentapp.data.model.Book
import com.shell.assessmentapp.data.model.House

@Dao
interface HouseDao {
    @Query("select * from houses_table order by name")
    suspend fun getAllHouses(): List<House>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(book: House)

    @Query("delete from books_table")
    suspend fun clean()

}