package com.shell.assessmentapp.data.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.shell.assessmentapp.data.model.converters.CustomConverter

@Entity(tableName = "charachter_table")
data class Charachter (
    @PrimaryKey(autoGenerate = false) @ColumnInfo(name = "_id") @Expose @SerializedName("id") val id: Int,
    @Expose @SerializedName("name") val name: String,
    @Expose @SerializedName("gender") val gender: String,
    @Expose @SerializedName("culture") val culture: String,
    @Expose @SerializedName("born") val born: String,
    @Expose @SerializedName("died") val died: String,
    @Expose @SerializedName("father") val father: String,
    @Expose @SerializedName("mother") val mother: String,
    @Expose @SerializedName("spouse") val spouse: String,
    @TypeConverters(CustomConverter::class) @Expose @SerializedName("titles") val titles: List<String>,
    @TypeConverters(CustomConverter::class) @Expose @SerializedName("aliases") val aliases: List<String>,
    @TypeConverters(CustomConverter::class) @Expose @SerializedName("allegiances") val allegiances: List<String>,
    @TypeConverters(CustomConverter::class) @Expose @SerializedName("playedBy") val playedBy: List<String>){

    override fun toString(): String {
        return "Charachter(${this.name}, ${this.gender})"
    }
}