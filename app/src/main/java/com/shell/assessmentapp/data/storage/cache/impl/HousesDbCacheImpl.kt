package com.shell.assessmentapp.data.storage.cache.impl

import android.content.Context
import com.shell.assessmentapp.data.model.House
import com.shell.assessmentapp.data.storage.cache.HousesDbCache
import com.shell.assessmentapp.data.storage.db.CategoryDatabase
import com.shell.assessmentapp.data.storage.db.HouseDao
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

object HousesDbCacheImpl: HousesDbCache {

    private var houseDao: HouseDao?=null
    private var houses: List<House>? = null

    fun getInstance(context: Context): HousesDbCache {
        houseDao = CategoryDatabase.getDatabase(context.applicationContext).getHousesDao()
        CoroutineScope(Dispatchers.IO).launch {
            houses = houseDao?.getAllHouses()
        }
        return this
    }

    override suspend fun getAllHouses(): List<House> {
        return houses!!
    }

    override suspend fun insertHouses(houses: List<House>) {
        houseDao?.clean()
        for(house in houses){
            houseDao?.insert(house)
        }
        this.houses = houseDao?.getAllHouses()
    }

    override suspend fun isDataCached(): Boolean {
        if(isNull()){
            return false
        }else{
            return true
        }
    }

    private fun isNull() = houses == null || houses?.size == 0
}