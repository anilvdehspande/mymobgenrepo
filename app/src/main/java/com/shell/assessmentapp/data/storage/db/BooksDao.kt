package com.shell.assessmentapp.data.storage.db

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.shell.assessmentapp.data.model.Book
import com.shell.assessmentapp.data.model.Category

@Dao
interface BooksDao {

    @Query("select * from books_table order by name")
    fun getAllBooks(): List<Book>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(book: Book)

    @Query("delete from books_table")
    suspend fun clean()

}
