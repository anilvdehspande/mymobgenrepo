package com.shell.assessmentapp.data.storage.db

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.shell.assessmentapp.data.model.Category
@Dao
interface CategoryDao {

    @Query("select * from category_table order by categoryName")
    suspend fun getAllCategories(): List<Category>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(category: Category)

    @Query("delete from category_table")
    suspend fun clean()
}