package com.shell.assessmentapp.data.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

@Entity(tableName = "category_table")
data class Category(@PrimaryKey(autoGenerate = true) @ColumnInfo(name = "_id")  val id: Int,
                    @Expose @SerializedName("category_name") val categoryName: String,
                    @Expose @SerializedName("type")val type: Int){

    override fun toString(): String {
        return "Category(${categoryName},${type})"
    }
}