package com.shell.assessmentapp.data.storage.cache

import com.shell.assessmentapp.data.model.Book
import com.shell.assessmentapp.data.model.Category

interface BookDbCache {
    suspend fun getAllBooks(): List<Book>
    suspend fun insertBooks(array: List<Book>)
    suspend fun isDataCached(): Boolean
}