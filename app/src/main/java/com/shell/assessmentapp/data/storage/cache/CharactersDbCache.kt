package com.shell.assessmentapp.data.storage.cache

import com.shell.assessmentapp.data.model.Charachter

interface CharactersDbCache {
    suspend fun getAllCharacters(): List<Charachter>
    suspend fun insertCharacters(characters: List<Charachter>)
    suspend fun isDataCached(): Boolean
}