package com.shell.assessmentapp.data.model.converters

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

class CustomConverter{
    @TypeConverter
    fun fromString(value: String): List<String>{
        val data = object : TypeToken<List<String>>(){

        }.type
        return  Gson().fromJson<List<String>>(value, data)
    }

    @TypeConverter
    fun fromList(list: List<String>): String {
        return Gson().toJson(list)
    }
}