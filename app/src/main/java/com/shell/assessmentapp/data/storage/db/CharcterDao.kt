package com.shell.assessmentapp.data.storage.db

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.shell.assessmentapp.data.model.Charachter

@Dao
interface CharcterDao {
    @Query("select * from charachter_table order by name")
    suspend fun getAllCharachters(): List<Charachter>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(charachter: Charachter)

    @Query("delete from charachter_table")
    suspend fun clean()
}