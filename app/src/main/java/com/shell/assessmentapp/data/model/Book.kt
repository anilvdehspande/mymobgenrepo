package com.shell.assessmentapp.data.model

import androidx.room.*
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.shell.assessmentapp.data.model.converters.CustomConverter

@Entity(tableName = "books_table")
data class Book(@PrimaryKey(autoGenerate = true) @ColumnInfo(name = "_id") val id: Int,
                @Expose @SerializedName("name") val name: String,
                @Expose @SerializedName("isbn") val isbn: String,
                @Expose @SerializedName("numberOfPages") val numberOfPages: Int,
                @Expose @SerializedName("publisher") val publisher: String,
                @Expose @SerializedName("country") val country: String,
                @Expose @SerializedName("mediaType") val mediaType: String,
                @Expose @SerializedName("released") val released: String,
                @TypeConverters(CustomConverter::class) @Expose @SerializedName("authors") val authors: List<String>) {

    override fun toString(): String {
        return "${this.name}, ${this.isbn}, ${this.publisher}, ${this.country},"
    }
}