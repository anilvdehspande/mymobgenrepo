package com.shell.assessmentapp.data.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

@Entity(tableName = "houses_table")
data class House (
    @PrimaryKey(autoGenerate = false) @ColumnInfo(name = "_id") @Expose @SerializedName("id") val id: Int,
    @Expose @SerializedName("name") val name: String,
    @Expose @SerializedName("region") val region: String,
    @Expose @SerializedName("title") val title: String){

    override fun toString(): String {
        return "${this.name}, ${this.region}, ${this.title}"
    }
}