package com.shell.assessmentapp.data.storage.cache.impl

import android.content.Context
import com.shell.assessmentapp.data.model.Charachter
import com.shell.assessmentapp.data.storage.cache.CharactersDbCache
import com.shell.assessmentapp.data.storage.db.CategoryDatabase
import com.shell.assessmentapp.data.storage.db.CharcterDao
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

object CharactersDbCacheImpl: CharactersDbCache {

    private var characterDao: CharcterDao? = null
    var characters: List<Charachter>? = null

    fun getInstance(context: Context): CharactersDbCache {
        characterDao = CategoryDatabase.getDatabase(context.applicationContext).getCharactersDao();
        CoroutineScope(Dispatchers.IO).launch {
            characters = characterDao?.getAllCharachters();
        }
        return this
    }

    override suspend fun getAllCharacters(): List<Charachter> {
        return characters!!
    }

    override suspend fun insertCharacters(characters: List<Charachter>) {
        characterDao?.clean()
        for(character in characters){
            characterDao?.insert(character)
        }
        this.characters = characterDao?.getAllCharachters()
    }

    override suspend fun isDataCached(): Boolean {
        if(isNull()){
            return false
        }else{
            return true
        }
    }

    private fun isNull() = characters == null || characters?.size == 0

}