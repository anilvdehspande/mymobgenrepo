package com.shell.assessmentapp.presentation.presenters

import com.shell.assessmentapp.data.model.Book
import com.shell.assessmentapp.data.model.Category
import com.shell.assessmentapp.presentation.presenters.base.BasePresenter
import com.shell.assessmentapp.presentation.ui.BaseView

interface BooksPresenter:BasePresenter {
    interface View: BaseView {
        fun displayBooks(books: List<Book>)
    }
}