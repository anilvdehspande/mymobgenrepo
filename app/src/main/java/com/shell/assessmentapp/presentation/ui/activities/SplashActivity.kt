package com.shell.assessmentapp.presentation.ui.activities


import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.shell.assessmentapp.R
import com.shell.assessmentapp.data.storage.cache.CategoryDbCache
import com.shell.assessmentapp.domain.executor.imp.ThreadExecutorSingleton
import com.shell.assessmentapp.domain.repository.imp.CategoryRepositoryImp
import com.shell.assessmentapp.domain.executor.imp.MainThreadSingleton
import com.shell.assessmentapp.domain.repository.CategoryRepository
import com.shell.assessmentapp.presentation.presenters.CategorySplashPresenter
import com.shell.assessmentapp.presentation.presenters.imp.CategorySplashPresenterImpl
import org.koin.android.ext.android.inject
import com.shell.assessmentapp.domain.executor.Executor
import com.shell.assessmentapp.domain.executor.MainThread

class SplashActivity: AppCompatActivity(), CategorySplashPresenter.View {

    val TAG = "SplashActivity"

    var categoryPresenter: CategorySplashPresenter? = null
    val categoryRepository: CategoryRepository by inject<CategoryRepository>()
    val mainThread: MainThread by inject()
    val executorThread: Executor by inject()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left)

        categoryPresenter = CategorySplashPresenterImpl(
            executorThread,
            mainThread, this,
            categoryRepository
        )
    }

    override fun onResume() {
        super.onResume()
        categoryPresenter?.resume()
    }

    override fun onPause() {
        super.onPause()
        categoryPresenter?.pause()
    }

    override fun onStop() {
        super.onStop()
        categoryPresenter?.stop()
    }

    override fun moveToNextScreen() {
        var activityIntent: Intent = Intent(this, MainActivity::class.java)
        startActivity(activityIntent)
        finish()
    }

    override fun showError(errorMessage: String) {
        Toast.makeText(this, errorMessage,Toast.LENGTH_SHORT).show()
    }

    override fun showProgress() {

    }

    override fun hideProgress() {

    }


}