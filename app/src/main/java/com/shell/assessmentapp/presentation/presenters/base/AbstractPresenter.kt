package com.shell.assessmentapp.presentation.presenters.base

import com.shell.assessmentapp.domain.executor.Executor
import com.shell.assessmentapp.domain.executor.MainThread

public abstract class AbstractPresenter(executor: Executor, mainThread: MainThread) {

    val executor: Executor? = null
    val mainThread: MainThread? = null
}