package com.shell.assessmentapp.presentation.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.shell.assessmentapp.R
import com.shell.assessmentapp.data.model.Charachter
import com.shell.assessmentapp.presentation.ui.adapter.listeners.OnItemClickListener
import kotlinx.android.synthetic.main.book_item_view.view.*

class CharachtersRecyclerAdapter(items: List<Charachter>, itemClickListener: OnItemClickListener?): RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var items = items

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return CharachterViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.book_item_view, parent, false)
        )
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when(holder){
            is CharachterViewHolder -> {
                holder.bind(items.get(position))
            }
        }
    }

    override fun getItemCount(): Int {
        return items.size
    }

    class CharachterViewHolder constructor(val view: View): RecyclerView.ViewHolder(view){
        val itemViewDetails = itemView.itemViewDetails

        fun bind(charachter: Charachter){
            itemViewDetails.text = "Name: ${charachter.name}\n" +
                                   "Aliases: ${charachter.aliases}" +
                                   "Gender: ${charachter.gender}\n" +
                                   "Culture: ${if (charachter.culture.isEmpty()) "No Info" else charachter.culture}\n" +
                                   "Mother: ${if (charachter.mother.isEmpty()) "No Info" else charachter.mother}\n" +
                                   "Spouse: ${if (charachter.spouse.isEmpty()) "No Info" else charachter.spouse }"
        }
    }


}