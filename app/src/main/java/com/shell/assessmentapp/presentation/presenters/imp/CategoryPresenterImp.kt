package com.shell.assessmentapp.presentation.presenters.imp

import com.shell.assessmentapp.domain.executor.Executor
import com.shell.assessmentapp.domain.executor.MainThread
import com.shell.assessmentapp.domain.interactor.CategoryInteractor
import com.shell.assessmentapp.domain.interactor.imp.CategoryInteractorImpl
import com.shell.assessmentapp.data.model.Category
import com.shell.assessmentapp.domain.repository.CategoryRepository
import com.shell.assessmentapp.presentation.presenters.CategoryPresenter
import com.shell.assessmentapp.presentation.presenters.base.AbstractPresenter

public class CategoryPresenterImp(executor: Executor,
                                  mainThread: MainThread,
                                  view: CategoryPresenter.View,
                                  repository: CategoryRepository, screenSelectionListener: CategoryPresenter.ScreenSelectionListener): AbstractPresenter(executor, mainThread),
                                                                   CategoryPresenter,
                                                                   CategoryInteractor.CallBack  {


    var view: CategoryPresenter.View? = view
    var repository: CategoryRepository = repository
    var interactor = CategoryInteractorImpl(executor!!,mainThread!!, this, this.repository)
    var screenSelectionListener: CategoryPresenter.ScreenSelectionListener? = screenSelectionListener

    override fun resume() {
        this.view?.showProgress()
        interactor.execute()
    }

    override fun pause() {

    }

    override fun stop() {

    }

    override fun destroy() {
    }

    override fun onError() {
        view?.showError("Something went wrong")
    }

    override fun onCategoriesReceived(categories: List<Category>) {
        view?.hideProgress()
        view?.displayCategories(categories)
    }

    override fun onCategoriesFailed(error: String) {
        view?.showError(error)
    }

    override fun addScreenSelectionListener(screenSelectionListener: CategoryPresenter.ScreenSelectionListener) {
        this.screenSelectionListener = screenSelectionListener
    }

    override fun onScreenSelected(string: String) {
        this.screenSelectionListener?.onScreenSelected(string)
    }
}