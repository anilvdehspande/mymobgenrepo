package com.shell.assessmentapp.presentation.presenters.imp

import com.shell.assessmentapp.data.model.Category
import com.shell.assessmentapp.domain.executor.Executor
import com.shell.assessmentapp.domain.executor.MainThread
import com.shell.assessmentapp.domain.interactor.CategoryInteractor
import com.shell.assessmentapp.domain.interactor.imp.CategoryInteractorImpl
import com.shell.assessmentapp.domain.repository.CategoryRepository
import com.shell.assessmentapp.presentation.presenters.CategoryPresenter
import com.shell.assessmentapp.presentation.presenters.CategorySplashPresenter
import com.shell.assessmentapp.presentation.presenters.base.AbstractPresenter

class CategorySplashPresenterImpl(executor: Executor,
                                  mainThread: MainThread,
                                  view: CategorySplashPresenter.View,
                                  repository: CategoryRepository) : CategorySplashPresenter, AbstractPresenter(executor, mainThread), CategoryInteractor.CallBack, CategoryRepository.CallBack {

    var view: CategorySplashPresenter.View? = view
    var repository: CategoryRepository = repository
    var interactor  = CategoryInteractorImpl(executor!!, mainThread!!, this, this.repository)

    override fun resume() {
        repository.setCallBackInstance(this)
        this.view?.showProgress()
        interactor.execute()
    }

    override fun pause() {

    }

    override fun stop() {

    }

    override fun destroy() {

    }

    override fun onError() {
        view?.showError("Something went wrong")
    }

    override fun onCategoriesReceived(categories: List<Category>) {
        view?.hideProgress()
        view?.moveToNextScreen()
    }

    override fun onCategoriesFailed(error: String) {
        view?.hideProgress()
        view?.showError(error)
    }

    override fun onFailure(errorMessage: String) {
        onCategoriesFailed(errorMessage)
    }

    override fun onSuccess(categories: List<Category>) {
        onCategoriesReceived(categories)
    }
}