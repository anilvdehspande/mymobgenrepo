package com.shell.assessmentapp.presentation.ui.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.shell.assessmentapp.R
import com.shell.assessmentapp.presentation.presenters.CategoryPresenter
import com.shell.assessmentapp.presentation.presenters.MainActivityPresenter
import com.shell.assessmentapp.presentation.presenters.imp.MainActivityPresenterImpl
import com.shell.assessmentapp.presentation.ui.fragments.BooksFragment
import com.shell.assessmentapp.presentation.ui.fragments.CategoryFragment
import com.shell.assessmentapp.presentation.ui.fragments.CharachtersFragment
import com.shell.assessmentapp.presentation.ui.fragments.HousesFragment
import org.koin.android.ext.android.inject
import org.koin.core.parameter.parametersOf

class MainActivity : AppCompatActivity(), MainActivityPresenter.View{

    val TAG = "MainActivity"

    val presenter: MainActivityPresenter? by inject{ parametersOf(this)}

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left)
        presenter?.onLoad()
    }

    private fun startFragment(fragment: Fragment){
        val fragmentManager = supportFragmentManager
        val fragmentTransaction = fragmentManager.beginTransaction()
        fragmentTransaction.replace(R.id.fragmentContainer, fragment)
        fragmentTransaction.commit()
        if(fragment !is CategoryFragment){
            fragmentTransaction.setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right)
            fragmentTransaction.addToBackStack(null)
        }
    }

    override fun onResume() {
        super.onResume()
        initHeader()
        presenter?.resume()
    }

    override fun onPause() {
        super.onPause()
        presenter?.pause()
    }

    override fun onStop() {
        super.onStop()
        presenter?.stop()
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter?.destroy()
    }

    override fun showCategoryScreen() {
        startFragment(CategoryFragment(presenter as CategoryPresenter.ScreenSelectionListener))
    }

    override fun showCharScreen() {
        startFragment(CharachtersFragment())
    }

    override fun showBookScreen() {
        startFragment(BooksFragment())
    }

    override fun showError(errorMessage: String) {
        Toast.makeText(this, errorMessage, Toast.LENGTH_LONG).show()
    }

    override fun showHousesScreen() {
        startFragment(HousesFragment())
    }

    override fun showProgress() {

    }

    override fun hideProgress() {

    }

    private fun initHeader() {
        this.title = getString(R.string.app_name)
    }
}
