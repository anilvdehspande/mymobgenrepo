package com.shell.assessmentapp.presentation.presenters.imp

import com.shell.assessmentapp.presentation.presenters.MainActivityPresenter

class MainActivityPresenterImpl(view: MainActivityPresenter.View) : MainActivityPresenter {

    var view: MainActivityPresenter.View? = view

    override fun onLoad() {
        view?.showCategoryScreen()
    }

    override fun resume() {

    }

    override fun pause() {

    }

    override fun stop() {

    }

    override fun destroy() {

    }

    override fun onError() {
        view?.showError("Something went wrong")
    }

    override fun onScreenSelected(string: String) {
        when(string){
            "Houses" -> {
                view?.showHousesScreen()
            }
            "Characters" -> {
                view?.showCharScreen()
            }
            "Books" -> {
                view?.showBookScreen()
            }
        }
    }
}