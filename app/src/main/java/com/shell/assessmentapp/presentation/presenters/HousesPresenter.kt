package com.shell.assessmentapp.presentation.presenters

import com.shell.assessmentapp.data.model.House
import com.shell.assessmentapp.presentation.presenters.base.BasePresenter
import com.shell.assessmentapp.presentation.ui.BaseView

interface HousesPresenter: BasePresenter {
    interface View: BaseView {
        fun displayBooks(houses: List<House>)
    }
}