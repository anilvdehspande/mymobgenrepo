package com.shell.assessmentapp.presentation.presenters.imp

import com.shell.assessmentapp.domain.executor.Executor
import com.shell.assessmentapp.domain.executor.MainThread
import com.shell.assessmentapp.domain.interactor.HousesInteractor
import com.shell.assessmentapp.domain.interactor.imp.HousesInteractorImp
import com.shell.assessmentapp.data.model.House
import com.shell.assessmentapp.domain.repository.HousesRepository
import com.shell.assessmentapp.presentation.presenters.HousesPresenter
import com.shell.assessmentapp.presentation.presenters.base.AbstractPresenter


class HousesPresenterImpl(executor: Executor,
                          mainThread: MainThread,
                          view: HousesPresenter.View,
                          repository: HousesRepository) : HousesPresenter, AbstractPresenter(executor, mainThread), HousesInteractor.Callback{

    var view: HousesPresenter.View? = view
    var repository: HousesRepository = repository
    var interactor = HousesInteractorImp(executor!!, mainThread!!, this, this.repository)


    override fun resume() {
        this.view?.showProgress()
        interactor.execute()
    }

    override fun pause() {

    }

    override fun stop() {

    }

    override fun destroy() {

    }

    override fun onError() {
        view?.hideProgress()
        view?.showError("Something went wrong")
    }

    override fun onHousesFailed(error: String) {
        view?.showError(error)
    }

    override fun onHousesReceived(houses: List<House>) {
        view?.hideProgress()
        view?.displayBooks(houses)
    }
}