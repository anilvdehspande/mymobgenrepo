package com.shell.assessmentapp.presentation.ui.fragments

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.shell.assessmentapp.R
import com.shell.assessmentapp.data.model.House
import com.shell.assessmentapp.domain.executor.Executor
import com.shell.assessmentapp.domain.executor.MainThread
import com.shell.assessmentapp.domain.executor.imp.ThreadExecutorSingleton
import com.shell.assessmentapp.domain.repository.imp.HousesRepositoryImpl
import com.shell.assessmentapp.presentation.presenters.HousesPresenter
import com.shell.assessmentapp.presentation.presenters.imp.HousesPresenterImpl
import com.shell.assessmentapp.domain.executor.imp.MainThreadSingleton
import com.shell.assessmentapp.domain.repository.HousesRepository
import com.shell.assessmentapp.presentation.ui.adapter.HousesRecyclerAdapter
import kotlinx.android.synthetic.main.fragment_books.*
import org.koin.android.ext.android.inject
import org.koin.core.parameter.parametersOf

class HousesFragment: Fragment(), HousesPresenter.View {

    private val TAG = "HousesFragment"

    val housesPresenter: HousesPresenter? by inject { parametersOf(this) }
    private lateinit var houseAdapter: HousesRecyclerAdapter


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_books, container, false)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    private fun initRecyclerView(houses: List<House>){
        commonRecyclerView.apply {
            layoutManager = LinearLayoutManager(activity)
            houseAdapter = HousesRecyclerAdapter(houses, null)
            adapter = houseAdapter
        }
    }

    override fun onResume() {
        super.onResume()
        initHeader()
        housesPresenter?.resume()
    }

    override fun onPause() {
        super.onPause()
        housesPresenter?.pause()
    }

    override fun onStop() {
        super.onStop()
        housesPresenter?.stop()
    }

    override fun onDestroy() {
        super.onDestroy()
        housesPresenter?.destroy()
    }

    override fun displayBooks(houses: List<House>) {
        Log.i(TAG," Displaying houses ${houses}")
        initRecyclerView(houses)
    }

    override fun showError(errorMessage: String) {
        Log.i(TAG,"Show Error ${errorMessage}")
    }

    override fun showProgress() {
        Log.i(TAG,"Show Progress")
        progress_circular.visibility = View.VISIBLE
        commonRecyclerView.visibility = View.GONE
    }

    override fun hideProgress() {
        Log.i(TAG,"Hiding Progress")
        progress_circular.visibility = View.GONE
        commonRecyclerView.visibility = View.VISIBLE
    }

    private fun initHeader() {
        (activity as AppCompatActivity).supportActionBar!!.title = getString(R.string.houses)
    }
}