package com.shell.assessmentapp.presentation.ui.adapter.listeners

interface OnItemClickListener {
    fun onItemClicked(position: Int)
}