package com.shell.assessmentapp.presentation.ui.fragments

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.shell.assessmentapp.R
import com.shell.assessmentapp.data.model.Charachter
import com.shell.assessmentapp.domain.executor.Executor
import com.shell.assessmentapp.domain.executor.MainThread
import com.shell.assessmentapp.domain.executor.imp.ThreadExecutorSingleton
import com.shell.assessmentapp.domain.repository.imp.CharsRepositoryImpl
import com.shell.assessmentapp.presentation.presenters.CharactersPresenter
import com.shell.assessmentapp.presentation.presenters.imp.CharactersPresenterImpl
import com.shell.assessmentapp.domain.executor.imp.MainThreadSingleton
import com.shell.assessmentapp.domain.repository.CharsRepository
import com.shell.assessmentapp.presentation.ui.adapter.CharachtersRecyclerAdapter
import kotlinx.android.synthetic.main.fragment_books.*
import org.koin.android.ext.android.inject
import org.koin.core.parameter.parametersOf

class CharachtersFragment: Fragment(), CharactersPresenter.View {

    val TAG = "CharachtersFragment"

    val presenter: CharactersPresenter? by inject { parametersOf(this) }
    private lateinit var charachterAdater: CharachtersRecyclerAdapter


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_books, container, false)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    private fun initRecyclerView(characters: List<Charachter>){
        commonRecyclerView.apply {
            layoutManager = LinearLayoutManager(activity)
            charachterAdater = CharachtersRecyclerAdapter(characters, null)
            adapter = charachterAdater
        }
    }

    override fun onResume() {
        initHeader()
        super.onResume()
        presenter?.resume()
    }

    override fun onPause() {
        super.onPause()
        presenter?.pause()
    }

    override fun onStop() {
        super.onStop()
        presenter?.stop()
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter?.destroy()
    }

    override fun displayCharachters(characters: List<Charachter>) {
        Log.i(TAG," Displaying books ${characters}")
        initRecyclerView(characters)
    }

    override fun showError(errorMessage: String) {
        Log.i(TAG,"Show Error ${errorMessage}")
    }

    override fun hideProgress() {
        Log.i(TAG,"Hiding Progress")
        progress_circular.visibility = View.GONE
        commonRecyclerView.visibility = View.VISIBLE
    }

    override fun showProgress() {
        Log.i(TAG,"Show Progress")
        progress_circular.visibility = View.VISIBLE
        commonRecyclerView.visibility = View.GONE
    }

    private fun initHeader() {
        (activity as AppCompatActivity).supportActionBar!!.title = getString(R.string.characters)
    }
}