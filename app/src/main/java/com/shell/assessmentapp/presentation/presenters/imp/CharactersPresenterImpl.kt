package com.shell.assessmentapp.presentation.presenters.imp

import com.shell.assessmentapp.domain.executor.Executor
import com.shell.assessmentapp.domain.executor.MainThread
import com.shell.assessmentapp.domain.interactor.CharsInteractor
import com.shell.assessmentapp.domain.interactor.imp.CharsInteractorImp
import com.shell.assessmentapp.data.model.Charachter
import com.shell.assessmentapp.domain.repository.CharsRepository
import com.shell.assessmentapp.presentation.presenters.CharactersPresenter
import com.shell.assessmentapp.presentation.presenters.base.AbstractPresenter

class CharactersPresenterImpl(executor: Executor,
                              mainThread: MainThread,
                              view: CharactersPresenter.View,
                              repository: CharsRepository) : CharactersPresenter, AbstractPresenter(executor, mainThread), CharsInteractor.Callback {


    var view: CharactersPresenter.View? = view
    var repository: CharsRepository = repository
    var interactor = CharsInteractorImp(executor!!, mainThread!!, this, this.repository)

    override fun resume() {

        this.view?.showProgress()
        interactor.execute()
    }

    override fun pause() {

    }

    override fun stop() {

    }

    override fun destroy() {

    }


    override fun onCharsFailed(error: String) {
        view?.hideProgress()
        view?.showError(error)
    }

    override fun onCharsReceived(charachters: List<Charachter>) {
        view?.hideProgress()
        view?.displayCharachters(charachters)
    }

    override fun onError() {
        view?.hideProgress()
        view?.showError("Something went wrong")
    }
}