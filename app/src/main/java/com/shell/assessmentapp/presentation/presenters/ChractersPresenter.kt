package com.shell.assessmentapp.presentation.presenters

import com.shell.assessmentapp.presentation.ui.BaseView
import com.shell.assessmentapp.data.model.Charachter
import com.shell.assessmentapp.presentation.presenters.base.BasePresenter

interface CharactersPresenter: BasePresenter {
    interface View: BaseView {
        fun displayCharachters(characters: List<Charachter>)
    }
}