package com.shell.assessmentapp.presentation.presenters.base

interface BasePresenter {
    fun resume()
    fun pause()
    fun stop()
    fun destroy()
    fun onError()
}