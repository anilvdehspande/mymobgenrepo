package com.shell.assessmentapp.presentation.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.shell.assessmentapp.data.model.Category
import kotlinx.android.synthetic.main.categoty_item_view.view.*
import com.shell.assessmentapp.R
import com.shell.assessmentapp.presentation.ui.adapter.listeners.OnItemClickListener


class CategoryAdapter(items: List<Category>, itemClickListener: OnItemClickListener): RecyclerView.Adapter<RecyclerView.ViewHolder>(){

    private var items  = items
    private var itemClickListener: OnItemClickListener? = itemClickListener

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return CategoryViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.categoty_item_view, parent, false)

        )
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when(holder){
            is CategoryViewHolder -> {
                holder.bind(items.get(position))
                holder.view.setOnClickListener(View.OnClickListener {
                    itemClickListener?.onItemClicked(position)
                })
            }
        }
    }

    override fun getItemCount(): Int {
        return items.size
    }

    class CategoryViewHolder constructor(
        val view: View
    ): RecyclerView.ViewHolder(view){

        val textViewCategoryName = itemView.textViewCategoryName

        fun bind(category: Category){
            textViewCategoryName.text = category.categoryName
        }
    }




}