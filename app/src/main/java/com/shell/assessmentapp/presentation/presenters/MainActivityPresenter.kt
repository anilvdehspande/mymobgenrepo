package com.shell.assessmentapp.presentation.presenters

import com.shell.assessmentapp.presentation.presenters.base.BasePresenter
import com.shell.assessmentapp.presentation.ui.BaseView

interface MainActivityPresenter: BasePresenter, CategoryPresenter.ScreenSelectionListener {

    fun onLoad()

    interface View: BaseView{
        fun showCategoryScreen()
        fun showCharScreen()
        fun showHousesScreen()
        fun showBookScreen()
    }
}