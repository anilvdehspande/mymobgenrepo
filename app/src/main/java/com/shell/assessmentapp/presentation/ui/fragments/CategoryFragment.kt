package com.shell.assessmentapp.presentation.ui.fragments

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.shell.assessmentapp.R
import com.shell.assessmentapp.data.model.Category
import com.shell.assessmentapp.data.storage.cache.CategoryDbCache
import com.shell.assessmentapp.presentation.presenters.CategoryPresenter
import com.shell.assessmentapp.presentation.presenters.imp.CategoryPresenterImp
import com.shell.assessmentapp.domain.repository.CategoryRepository
import com.shell.assessmentapp.presentation.ui.adapter.CategoryAdapter
import com.shell.assessmentapp.presentation.ui.adapter.listeners.OnItemClickListener
import kotlinx.android.synthetic.main.fragment_category.*
import org.koin.android.ext.android.inject
import com.shell.assessmentapp.domain.executor.Executor
import com.shell.assessmentapp.domain.executor.MainThread
import org.koin.core.parameter.parametersOf

class CategoryFragment(screenSelectionListener: CategoryPresenter.ScreenSelectionListener): Fragment(), CategoryPresenter.View, OnItemClickListener {

    val TAG = "CategoryFragment"

    var categoryPresenter: CategoryPresenter? = null
    private lateinit var categoryAdapter: CategoryAdapter
    var screenSelectionListener = screenSelectionListener
    val categoryRepository: CategoryRepository by inject<CategoryRepository>()
    val mainThread: MainThread by inject()
    val executorThread: Executor by inject()


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_category, container, false)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        categoryPresenter =
            CategoryPresenterImp(executorThread, mainThread, this, categoryRepository, screenSelectionListener)
    }

    private fun initRecyclerView(categories: List<Category>){
        categoryRecyclerView.apply {
            layoutManager = LinearLayoutManager(activity)
            categoryAdapter = CategoryAdapter(categories, this@CategoryFragment)
            adapter = categoryAdapter
        }
    }

    override fun onResume() {
        super.onResume()
        initHeader()
        categoryPresenter?.resume()
    }

    override fun onPause() {
        super.onPause()
        categoryPresenter?.pause()
    }

    override fun onStop() {
        super.onStop()
        categoryPresenter?.stop()
    }

    override fun displayCategories(categories: List<Category>) {
        Log.i(TAG,"Got the categories:+ ${categories} ")
        initRecyclerView(categories)
    }

    override fun showProgress() {
        Log.i(TAG,"Show Progress")
        progress_circular?.visibility = View.VISIBLE
        categoryRecyclerView?.visibility = View.INVISIBLE
    }

    override fun showError(errorMessage: String) {
        Toast.makeText(activity!!, errorMessage, Toast.LENGTH_SHORT).show()
    }

    override fun hideProgress() {
        Log.i(TAG,"Hiding Progress")
        progress_circular?.visibility = View.GONE
        categoryRecyclerView?.visibility = View.VISIBLE
    }

    override fun onDestroy() {
        super.onDestroy()
        categoryPresenter?.destroy()
    }

    override fun onItemClicked(position: Int) {
        when(position){
            0 -> {
                categoryPresenter?.onScreenSelected(getString(R.string.books))
            }
            1 ->{
                categoryPresenter?.onScreenSelected(getString(R.string.characters))
            }
            2 -> {
                categoryPresenter?.onScreenSelected(getString(R.string.houses))
            } else -> {
                Log.i(TAG,"The position clicked ${position}, You can't be here at all")
            }
        }

    }

    private fun initHeader() {
        (activity as AppCompatActivity).supportActionBar!!.title = getString(R.string.categories)
    }

}