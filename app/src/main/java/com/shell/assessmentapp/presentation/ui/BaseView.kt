package com.shell.assessmentapp.presentation.ui

interface BaseView {
    fun showProgress()
    fun hideProgress()
    fun showError(errorMessage: String)
}