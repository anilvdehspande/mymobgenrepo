package com.shell.assessmentapp.presentation.ui.fragments

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.shell.assessmentapp.R
import com.shell.assessmentapp.data.model.Book
import com.shell.assessmentapp.domain.executor.Executor
import com.shell.assessmentapp.domain.executor.MainThread
import com.shell.assessmentapp.domain.executor.imp.ThreadExecutorSingleton
import com.shell.assessmentapp.domain.repository.imp.BookRepositoryImp
import com.shell.assessmentapp.presentation.presenters.BooksPresenter
import com.shell.assessmentapp.presentation.presenters.imp.BooksPresenterImpl
import com.shell.assessmentapp.domain.executor.imp.MainThreadSingleton
import com.shell.assessmentapp.domain.repository.BooksRepository
import com.shell.assessmentapp.presentation.ui.adapter.BooksRecyclerAdapter
import kotlinx.android.synthetic.main.fragment_books.*
import org.koin.android.ext.android.inject
import org.koin.core.parameter.parametersOf

class BooksFragment: Fragment(), BooksPresenter.View {

    val TAG = "BooksFragment"

    val bookPresenter: BooksPresenter? by inject { parametersOf(this) }
    private lateinit var bookAdapter: BooksRecyclerAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_books, container, false)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    private fun initRecyclerView(books: List<Book>){
        commonRecyclerView.apply {
            layoutManager = LinearLayoutManager(activity)
            bookAdapter = BooksRecyclerAdapter(books, null)
            adapter = bookAdapter
        }
    }

    override fun onResume() {
        super.onResume()
        initHeader()
        bookPresenter?.resume()
    }

    override fun onPause() {
        super.onPause()
        bookPresenter?.pause()
    }

    override fun onStop() {
        super.onStop()
        bookPresenter?.stop()
    }

    override fun onDestroy() {
        super.onDestroy()
        bookPresenter?.destroy()
    }

    override fun displayBooks(books: List<Book>) {
        Log.i(TAG," Displaying books ${books}")
        initRecyclerView(books)
    }

    override fun showError(errorMessage: String) {
        Log.i(TAG,"Show Error ${errorMessage}")
    }

    override fun showProgress() {
        Log.i(TAG,"Show Progress")
        progress_circular.visibility = View.VISIBLE
        commonRecyclerView.visibility = View.GONE
    }

    override fun hideProgress() {
        Log.i(TAG,"Hiding Progress")
        progress_circular.visibility = View.GONE
        commonRecyclerView.visibility = View.VISIBLE
    }
    
    private fun initHeader() {
        (activity as AppCompatActivity).supportActionBar!!.title = getString(R.string.books)
    }

}