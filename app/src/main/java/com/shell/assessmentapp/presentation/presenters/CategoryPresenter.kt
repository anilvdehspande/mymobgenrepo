package com.shell.assessmentapp.presentation.presenters

import com.shell.assessmentapp.data.model.Category
import com.shell.assessmentapp.presentation.presenters.base.BasePresenter
import com.shell.assessmentapp.presentation.ui.BaseView

public interface CategoryPresenter: BasePresenter {

    interface ScreenSelectionListener {
        fun onScreenSelected(string: String)
    }

    interface View: BaseView{
        fun displayCategories(categories: List<Category>)
    }

    fun addScreenSelectionListener(screenSelectionListener: ScreenSelectionListener)
    fun onScreenSelected(string: String)

}