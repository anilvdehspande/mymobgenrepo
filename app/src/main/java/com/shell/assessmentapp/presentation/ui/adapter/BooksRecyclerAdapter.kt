package com.shell.assessmentapp.presentation.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.shell.assessmentapp.R
import com.shell.assessmentapp.data.model.Book
import com.shell.assessmentapp.presentation.ui.adapter.listeners.OnItemClickListener
import kotlinx.android.synthetic.main.book_item_view.view.*

class BooksRecyclerAdapter(items: List<Book>, itemClickListener: OnItemClickListener?): RecyclerView.Adapter<RecyclerView.ViewHolder>(){
    private var items  = items

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return CategoryViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.book_item_view, parent, false)
        )
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when(holder){
            is CategoryViewHolder -> {
                holder.bind(items.get(position))
            }
        }
    }

    override fun getItemCount(): Int {
        return items.size
    }

    class CategoryViewHolder constructor(val view: View): RecyclerView.ViewHolder(view){

        val itemViewDetails = itemView.itemViewDetails

        fun bind(book: Book){
            itemViewDetails.text = "Name: ${book.name}\n" +
                                    "ISBN: ${book.isbn}\n" +
                                    "Authors${book.authors}\n" +
                                    "Country: ${book.country}\n" +
                                    "Media Type: ${book.mediaType}\n" +
                                    "Number of Pages: ${book.numberOfPages}\n" +
                                    "Publisher: ${book.publisher}\n" +
                                    "ReleaseDate: ${book.released}"
        }
    }
}