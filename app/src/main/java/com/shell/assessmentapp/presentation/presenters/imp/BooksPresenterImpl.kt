package com.shell.assessmentapp.presentation.presenters.imp

import com.shell.assessmentapp.domain.executor.Executor
import com.shell.assessmentapp.domain.executor.MainThread
import com.shell.assessmentapp.domain.interactor.BooksInteractor
import com.shell.assessmentapp.domain.interactor.imp.BooksInteractorImp
import com.shell.assessmentapp.data.model.Book
import com.shell.assessmentapp.domain.repository.BooksRepository
import com.shell.assessmentapp.presentation.presenters.BooksPresenter
import com.shell.assessmentapp.presentation.presenters.base.AbstractPresenter
import org.koin.core.KoinComponent
import org.koin.core.inject

class BooksPresenterImpl(executor: Executor,
                         mainThread: MainThread,
                         view: BooksPresenter.View,
                         repository: BooksRepository) : BooksPresenter, AbstractPresenter(executor, mainThread), BooksInteractor.Callback, KoinComponent {

    val view: BooksPresenter.View?  = view
    val repository: BooksRepository by inject()
    val interactor = BooksInteractorImp(executor!!, mainThread!!, this, this.repository)

    override fun resume() {
        this.view?.showProgress()
        interactor.execute()
    }

    override fun pause() {

    }

    override fun stop() {

    }

    override fun destroy() {

    }

    override fun onError() {
        view?.hideProgress()
        view?.showError("Something went wrong")
    }

    override fun onBooksFailed(error: String) {
        view?.hideProgress()
        view?.showError(error)
    }

    override fun onBooksReceived(books: List<Book>) {
        view?.hideProgress()
        view?.displayBooks(books)
    }


}