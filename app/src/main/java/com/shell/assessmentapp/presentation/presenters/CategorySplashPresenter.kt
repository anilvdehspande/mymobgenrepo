package com.shell.assessmentapp.presentation.presenters

import com.shell.assessmentapp.presentation.presenters.base.BasePresenter
import com.shell.assessmentapp.presentation.ui.BaseView

interface CategorySplashPresenter: BasePresenter {
    interface View: BaseView {
        fun moveToNextScreen()
    }
}