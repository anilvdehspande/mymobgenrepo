package com.shell.assessmentapp.presentation.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.shell.assessmentapp.R
import com.shell.assessmentapp.data.model.House
import com.shell.assessmentapp.presentation.ui.adapter.listeners.OnItemClickListener
import kotlinx.android.synthetic.main.book_item_view.view.*

class HousesRecyclerAdapter(items: List<House>, itemClickListener: OnItemClickListener?): RecyclerView.Adapter<RecyclerView.ViewHolder>(){

    private var items = items

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return HouseViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.book_item_view, parent, false)
        )
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int){
        when (holder){
            is HouseViewHolder -> {
                holder.bind(items.get(position))
            }
        }
    }

    override fun getItemCount(): Int {
        return items.size
    }

    class HouseViewHolder constructor(val view: View): RecyclerView.ViewHolder(view){
        val itemViewDetails = itemView.itemViewDetails

        fun bind(house: House){
            itemViewDetails.text = "Name: ${house.name}\n" +
                                   "Region: ${house.region}\n" +
                                   "Title: ${house.title}"
        }
    }
}