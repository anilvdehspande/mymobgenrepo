package com.shell.assessmentapp.ui

import android.content.Intent
import androidx.test.espresso.Espresso
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.filters.LargeTest
import androidx.test.rule.ActivityTestRule
import androidx.test.runner.AndroidJUnit4
import com.shell.assessmentapp.presentation.ui.activities.SplashActivity
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import androidx.test.espresso.assertion.ViewAssertions.matches
import com.shell.assessmentapp.R
import com.shell.assessmentapp.presentation.ui.activities.MainActivity
import androidx.test.espresso.action.ViewActions.click

@RunWith(AndroidJUnit4::class)
@LargeTest
class MainScreenTest {
    private lateinit var bookString: String
    private lateinit var charachterString: String
    private lateinit var housesString: String

    @get: Rule
    var actvityRule: ActivityTestRule<MainActivity>  = ActivityTestRule(MainActivity::class.java)

    @Before
    fun setUp(){
        bookString = "Books"
        charachterString = "Characters"
        housesString = "Houses"
        var activityIntent = Intent()
        actvityRule.launchActivity(activityIntent)
    }

    @Test
    fun test_mainactivity_shouldConrainListView(){
        Espresso.onView(withId(R.id.categoryRecyclerView)).check(matches(isDisplayed()))
    }

    @Test
    fun test_mainactivity_shouldContainBooksText(){
        Espresso.onView(withText(bookString)).check(matches(isDisplayed()))
    }

    @Test
    fun test_mainactivity_shouldContainCharactersText(){
        Espresso.onView(withText(charachterString)).check(matches(isDisplayed()))
    }

    @Test
    fun test_mainactivity_shouldContrainHousesText(){
        Espresso.onView(withText(housesString)).check(matches(isDisplayed()))
    }


}