package com.shell.assessmentapp.ui

import androidx.test.espresso.Espresso
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.filters.LargeTest
import androidx.test.rule.ActivityTestRule
import androidx.test.runner.AndroidJUnit4
import com.shell.assessmentapp.presentation.ui.activities.SplashActivity
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import androidx.test.espresso.assertion.ViewAssertions.matches
import com.shell.assessmentapp.R


@RunWith(AndroidJUnit4::class)
@LargeTest
class SplashScreenTest {

    private lateinit var stringToBetyped: String

    @Before
    fun setUp(){
        stringToBetyped = "Welcome to Assignment"
    }

    @get: Rule
    var activityRule: ActivityTestRule<SplashActivity> = ActivityTestRule(SplashActivity::class.java)

    @Test
    fun test_splash_screen_shouldShow_messageView(){
        Espresso.onView(withId(R.id.splashMessage)).check(matches(isDisplayed()))
    }

    @Test
    fun test_splash_screen_shouldShowCorrectMessage(){
        Espresso.onView(withText(stringToBetyped)).check(matches(isDisplayed()))
    }
}