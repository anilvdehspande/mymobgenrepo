package com.shell.assessmentapp.domain.repository.imp

import com.shell.assessmentapp.appModule
import com.shell.assessmentapp.data.model.Book
import com.shell.assessmentapp.data.storage.cache.BookDbCache
import com.shell.assessmentapp.domain.repository.BooksRepository
import com.shell.assessmentapp.executorsModule
import com.shell.assessmentapp.presentationModule
import com.shell.assessmentapp.repositoryModule
import io.mockk.*
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

import org.junit.Assert.*
import org.hamcrest.CoreMatchers.*
import org.junit.After
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.loadKoinModules
import org.koin.core.context.startKoin
import org.koin.test.KoinTest


class BookRepositoryImpTest: KoinTest {

    lateinit var SUT: BookRepositoryImp
    lateinit var mockBookDbCache: BookDbCache
    lateinit var mockCallBack: BooksRepository.CallBack
    lateinit var books: List<Book>

    @Before
    fun setUp() {
        startKoin {
            androidContext(mockk())
            loadKoinModules(listOf(appModule, repositoryModule, executorsModule, presentationModule))
        }
        mockBookDbCache = mockk<BookDbCache>()
        mockCallBack = mockk<BooksRepository.CallBack>()
        SUT = BookRepositoryImp(mockBookDbCache)
    }

    @Test
    fun `test getBooks should invoke isDataCaachedMethod at least once`(){
        spyk<BookDbCache>()
        SUT.setCallBackInstance(mockCallBack)
        coEvery { mockBookDbCache.getAllBooks() } returns listOf()
        SUT.getBooks()
        coVerify { mockBookDbCache.isDataCached() }
    }

    @Test
    fun `test getBooks should not invoke callback success when callback not set`(){
        spyk<BookDbCache>()
        coEvery { mockBookDbCache.getAllBooks() } returns listOf()
        SUT.getBooks()
        coVerify { mockCallBack wasNot Called }
    }

    @Test
    fun `test getBooks should invoke callback success when callback set`(){
        spyk<BookDbCache>()
        SUT.setCallBackInstance(mockCallBack)
        coEvery { mockBookDbCache.getAllBooks() } returns listOf()
        SUT.getBooks()
        coVerify { mockCallBack.onSuccess(any()) }
    }



    @After
    fun tearDown() {
    }

}