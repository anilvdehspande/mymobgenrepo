package com.shell.assessmentapp.domain.interactor.imp

import com.shell.assessmentapp.appModule
import com.shell.assessmentapp.domain.executor.Executor
import com.shell.assessmentapp.domain.executor.MainThread
import com.shell.assessmentapp.domain.interactor.BooksInteractor
import com.shell.assessmentapp.domain.repository.BooksRepository
import com.shell.assessmentapp.executorsModule
import com.shell.assessmentapp.presentationModule
import com.shell.assessmentapp.repositoryModule
import io.mockk.every
import io.mockk.mockk
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.Assert.*
import org.hamcrest.CoreMatchers.*
import org.koin.test.KoinTest
import org.junit.After
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.loadKoinModules
import org.koin.core.context.startKoin


class BooksInteractorImpTest: KoinTest {

    lateinit var SUT: BooksInteractorImp
    lateinit var mockExecutor: Executor
    lateinit var mockMainThread: MainThread
    lateinit var mockCallBack: BooksInteractor.Callback
    lateinit var mockBookRepository: BooksRepository

    @Before
    fun setUp() {
        startKoin {
            androidContext(mockk())
            loadKoinModules(listOf(appModule, repositoryModule, executorsModule, presentationModule))
        }
        mockExecutor = mockk<Executor>()
        mockMainThread = mockk<MainThread>()
        mockCallBack = mockk<BooksInteractor.Callback>()
        mockBookRepository = mockk<BooksRepository>()

        SUT = BooksInteractorImp(mockExecutor, mockMainThread, mockCallBack,mockBookRepository)

    }

    @Test
    fun `test run method has not call back if no call back is set`(){
        //Arrange
        every { mockExecutor.execute(SUT) } returns Unit
        every { mockMainThread.post(Thread()) } returns Unit
        every { mockCallBack.onBooksFailed("Something went wrong") } returns Unit
        every { mockBookRepository.getBooks() } returns
        //Act
        SUT.run()
        //Assert

    }

    @After
    fun tearDown() {
    }
}