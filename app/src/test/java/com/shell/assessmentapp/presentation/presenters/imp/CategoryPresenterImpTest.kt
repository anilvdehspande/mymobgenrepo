package com.shell.assessmentapp.presentation.presenters.imp

import com.shell.assessmentapp.domain.executor.Executor
import com.shell.assessmentapp.domain.executor.MainThread
import com.shell.assessmentapp.domain.executor.imp.MainThreadSingleton
import com.shell.assessmentapp.domain.executor.imp.ThreadExecutorSingleton
import com.shell.assessmentapp.domain.repository.CategoryRepository
import com.shell.assessmentapp.presentation.presenters.CategoryPresenter
import io.mockk.*
import org.hamcrest.CoreMatchers
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.Assert.*
import org.hamcrest.CoreMatchers.*

class CategoryPresenterImpTest {

    lateinit var SUT: CategoryPresenterImp

    lateinit var mockRepository: CategoryRepository
    lateinit var view: CategoryPresenter.View
    lateinit var mockScreenSelectionListener: CategoryPresenter.ScreenSelectionListener

    lateinit var mainThread: MainThread
    lateinit var executor: Executor

    @Before
    fun setUp() {
        view = mockk<CategoryPresenter.View>()
        mockRepository = mockk<CategoryRepository>()
        mockScreenSelectionListener = mockk<CategoryPresenter.ScreenSelectionListener>()

        SUT = CategoryPresenterImp(ThreadExecutorSingleton, MainThreadSingleton,view,mockRepository,mockScreenSelectionListener)
    }

    //Method getMainLooper in android.os.Looper not mocked exception is being thrown
    @Test
    fun test_when_call_back_is_not_set_presentes_callback_is_not_triggered(){
        //Call back is not set
        every { mockRepository.getCategories()} returns Unit
        spyk<CategoryRepository>(mockRepository)
        mockRepository.getCategories()
        confirmVerified(mockScreenSelectionListener)
    }

    //When repository returns nothing, then
}