package com.shell.assessmentapp.data.storage.cache.impl

import com.shell.assessmentapp.*
import com.shell.assessmentapp.data.storage.db.BooksDao
import com.shell.assessmentapp.data.storage.db.CategoryDao
import io.mockk.mockk
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import com.shell.assessmentapp.data.model.Book
import io.mockk.coEvery
import io.mockk.every
import kotlinx.coroutines.CoroutineScope

import org.junit.Assert.*
import org.hamcrest.CoreMatchers.*
import org.junit.After
import org.koin.android.ext.koin.androidApplication
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.loadKoinModules
import org.koin.core.context.startKoin
import org.koin.dsl.koinApplication
import org.koin.test.KoinTest


class BookDbCacheImplTest: KoinTest {

    lateinit var SUT: BookDbCacheImpl
    lateinit var bookDao: BooksDao
    lateinit var books: List<Book>


    @Before
    fun setUp() {

        startKoin {
            androidContext(mockk())
            loadKoinModules(listOf(appModule, repositoryModule, executorsModule, presentationModule))
        }
        bookDao = mockk()
        SUT = BookDbCacheImpl(bookDao)
    }

    @After
    fun tearDown(){

    }

    @Test
    fun `getAllBooks returns empty list when uninitialized`() {
        //Arrange
        every { bookDao.getAllBooks() } returns listOf()
        //Act

        runBlocking {

            books = SUT.getAllBooks()
        }
        //Assert
        assertEquals(books.size, 0)
    }

    @Test
    fun `getAllBooks should return single book if dao returns single book`() {
        //Arrange
        var book  = Book(1,
            "SampleBooks",
            "ISBN",
            100,
            "Publisher",
            "Country",
            "MediaType",
            "Released",
            listOf("Author1", "Author2"))

        every { bookDao.getAllBooks() } returns listOf(book)

        //Act
        runBlocking {

            books = SUT.getAllBooks()
        }

        //Assert
        assertEquals(books.get(0), book)
    }
}