This is sample Android application that has been created for the purpose of assignment
submission for the mobgen.

This Android project has been developed in Kotlin.

The Application has been developed adhering to the clean code architecture.
The application contains single module. This has been done intentionally to keep the code simple.

The Application flow is as follows

1. The application launches with a Splash Screen
    a. This has Lottie animation
    b. In this screen, during the animation, First API call to Categories is made
    c. The Response from the categories is stored in the Local Room Database
    d. After the response is stored, the app navigates to Categories Screen.
    e. We us animation to move from Splash Screen to Categories Screen

2. In Categories Screen, you can see three options  - Books, Characters, Houses
    a. This is built using simple Activity, with multiple Fragments
    b. Each of these items are part of custom RecyclerView

3.On Click on each these items, you are navigated to different screen, which is a Fragment
    a. First time an API call is made, to get the response from back end
    b. Response is stored in the local database
    c. Next time onwards, data is always read from the local Room Database.


Code Architecture

The application follows clean code Architecture (without multiple modules - to avoid Data mapping between modules)
It has three major packages
1. data - Main data module, contains bean and local Room database related code
    a. model - Bean classes
    b. storage - Contains DAOs and DB Caches implementation
2. domain - This module contains repositories, APIs, interactor and multithreading logic
    a. api - Contains Retrofit logic to make RestFul API calls
    b. executor - Contains custom classes to do the context switch between thread and main thread
    c. interactor - Main interaction Point with Repositories
    d. repository - Contains multiple repositories that decide whether to make API call or fetch it from local Room Database
3. presentation - the front end of the application. This has been built using View & Presenter Pattern
    a. presenters - defined presenters
    b. ui - defined the activities and Fragments which implement the view in presenter
4. AppModule - This defined the dependencies using Koin library
5. MyApplication - Custom Application class which loads the dependecies.


The Flow is
View -> Presenter -> Interactor -> Repository -> (API Call OR Room DB)
There are call backs
    From Repository to Interactor
    From Interactor to Presenter
    From Presenter to View.
Each outer module is aware on only it's inner module.

DI is achived through Koin.


Unit Tests are written suing KoinTest fragmework instead of Mockito.
Unit Tests are written for Book module only.
That too for limited number of classes.

UI Tests are written using Espresso

What I did learn.
    Clean Code Architecture, Coroutines, Kotlin, DI using Koin, Organizing the code as per Clean code Architecture
    KoinTest

Future Scope
    Since developed this project with limited time at my hand, I couldn't explore coroutines to further extent.
    How to moc, spy some of the KoinTest scenarios and also some of the unique matachers in Koin
    



